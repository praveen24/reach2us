<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct() {
        parent::__construct();
    }


	public function index()
	{
		$this->data['view_page'] = 'home/index';
        $this->load->view('home/template', $this->data);
	}

	public function about()
	{
		$this->data['view_page'] = 'home/about';
        $this->load->view('home/template', $this->data);
	}

	public function contact()
	{
		$this->data['view_page'] = 'home/contact';
        $this->load->view('home/template', $this->data);
	}

	public function privacy()
	{
		$this->data['view_page'] = 'home/privacy';
        $this->load->view('home/template', $this->data);
	}

	public function terms()
	{
		$this->data['view_page'] = 'home/terms';
        $this->load->view('home/template', $this->data);
	}

	public function listing()
	{
		$this->data['view_page'] = 'home/listing';
        $this->load->view('home/template', $this->data);
	}

	public function company()
	{
		$this->data['view_page'] = 'home/company';
        $this->load->view('home/template', $this->data);
	}

	public function forgot_password()
	{
		$this->data['view_page'] = 'home/forgot_password';
        $this->load->view('home/template', $this->data);
	}
}