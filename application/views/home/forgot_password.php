
		<section>
			<div class="container">
				<div class="row">
					<div class="col-lg-6 offset-lg-3">
						<div class="forgot_pswrd text-center">
							<div class="login_logo">
	      						<img src="<?= base_url('assets/img/logo.png') ?>" class="img-fluid mx-auto d-block">
	      					</div>
							<h4>FORGOT PASSWROD?</h4>
							<p>Enter your registered Email address below to reset your password.</p>
							<form>
								<input type="email" name="email" class="form-control" placeholder="Email">
								<button class="btn btn-log float-right">SUBMIT</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
		