
		<section>
			<div class="container">
				<div class="row contact_section">
					<div class="col-lg-8 col-md-8 col-sm-12 col-12">
						<h4>SEND A MESSAGE</h4>
						<hr>
						<div class="clearfix"></div><br>
						<form>
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12 col-12">
									<div class="form-group">
										<input type="text" name="name" placeholder="Name" class="form-control">
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12 col-12">
									<div class="form-group">
										<input type="email" name="email" placeholder="Email" class="form-control">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12 col-12">
									<div class="form-group">
										<input type="text" name="phone" placeholder="Phone Number" class="form-control">
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12 col-12">
									<div class="form-group">
										<input type="text" name="subject" placeholder="Subject" class="form-control">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<div class="form-group">
										<textarea class="form-control" name="message" placeholder="Message" rows="5"></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-12">
									<div class="form-group">
										<button class="btn btn-contact float-right">SEND</button>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-12">
						<h4>FOLLOW US</h4>
						<hr>
						<div class="clearfix"></div><br>
						<a href="#" class="btn btn-social btn-fb btn-block">
						    <i class="fa fa-facebook pr-1"></i>
						    <span>Facebook</span>
						</a>
						<a href="#" class="btn btn-social btn-tw btn-block">
						    <i class="fa fa-twitter pr-1"></i>
						    <span>Twitter</span>
						</a>
						<a href="#" class="btn btn-social btn-google btn-block">
						    <i class="fa fa-google-plus pr-1"></i>
						    <span>Google+</span>
						</a>
						<a href="#" class="btn btn-social btn-instagram btn-block">
						    <i class="fa fa-instagram pr-1"></i>
						    <span>Instagram</span>
						</a>
						<a href="#" class="btn btn-social btn-linkedin btn-block">
						    <i class="fa fa-linkedin pr-1"></i>
						    <span>LinkedIn</span>
						</a>
					</div>
				</div>
			</div>
			<div class="container-fluid">
				<div class="row contact_details">
					<div class="col-12">
						<div class="container">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-sm-12 col-12">
									<div class="row no-margin">
										<div class="col-lg-3">
											<i class="fa fa-road"></i>
										</div>
										<div class="col-lg-9">
											<h4>ADDRESS</h4>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's.</p>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-12 col-12">
									<div class="row no-margin">
										<div class="col-lg-3">
											<i class="fa fa-phone"></i>
										</div>
										<div class="col-lg-9">
											<h4>PHONE</h4>
											<p>Phone: (01) 492910 341</p>
											<p>Mobile: (01) 492910 341</p>
											<p>Fax: (01) 492910 341</p>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-12 col-12">
									<div class="row no-margin">
										<div class="col-lg-3">
											<i class="fa fa-envelope"></i>
										</div>
										<div class="col-lg-9">
											<h4>EMAIL</h4>
											<p>info@reach2us.com</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3929.4701455417935!2d76.29479901434247!3d9.977967292866243!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3b080d35b5772b59%3A0x4dd432e402832f80!2sSoarMorrow+Solutions+Pvt.+Ltd.!5e0!3m2!1sen!2sin!4v1535614003526" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		