		<section id="mainSlider">
			<div id="owl-main" class="owl-carousel height-md owl-inner-nav owl-ui-lg">
				<div class="item" style="background-image: url(<?= base_url('assets/img/slider.jpg') ?>);">
					<div class="container">
						<div class="caption vertical-center text-center">
							
							<div class="fadeInDown-1 main-heading">
								<h1 class="light-color">CREATE YOUR BUSINESS PROFILE</h1>
							</div>
							<div class="fadeInDown-2 sub-heading">
								<p class="medium-color">
									<strong>GROW WITH 
										<span>REACH<span class="two">2</span>US
										</span>
									</strong>
								</p>
							</div>
						</div><!-- /.caption -->
					</div><!-- /.container -->
				</div><!-- /.item -->
				<div class="item" style="background-image: url(<?= base_url('assets/img/slider.jpg') ?>);">
					<div class="container">
						<div class="caption vertical-center text-center">
							<div class="fadeInDown-1 main-heading">
								<h1 class="light-color">CREATE YOUR BUSINESS PROFILE</h1>
							</div>
							<div class="fadeInDown-2 sub-heading">
								<p class="medium-color">
									<strong>GROW WITH 
										<span>REACH<span class="two">2</span>US
										</span>
									</strong>
								</p>
							</div>
							
						</div><!-- /.caption -->
					</div><!-- /.container -->
				</div><!-- /.item -->
			</div><!-- /.owl-carousel -->
		</section>
		<div class="container-fluid">
			<div class="row search">
				<div class="col-lg-12">
					<div class="row">
						<div class="col-lg-6 offset-lg-3 col-md-12 col-sm-12 col-12">
							<div class="search_area">
								<form action="">
									<input type="text" name="search" placeholder="Search by: State, District, City" class="form-control">
									<i class="fa fa-search"></i>
									<button class="btn btn-search">Search</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row">
				<div id="owl-demo">
    				<div class="item">
    					<div class="category1 category">
    						<div data-target="#categoryModal" data-toggle="modal">
    							<img src="<?= base_url('assets/img/icon1.png') ?>"><span>Entertainment</span>
    						</div>
    					</div>
    					<div class="category2 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon4.png') ?>"><span>Valuable</span>
    						</div>
    					</div>
    					<div class="category1 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon3.png') ?>"><span>Accessories</span>
    						</div>
    					</div>
    					<div class="category2 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon5.png') ?>"><span>Media</span>
    						</div>
    					</div>
    					<div class="category1 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon6.png') ?>"><span>Academics & Books</span>
    						</div>
    					</div>
    				</div>
    				<div class="item">
    					<div class="category3 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon2.png') ?>"><span>Valuable</span>
    						</div>
    					</div>
    					<div class="category4 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon6.png') ?>"><span>Academics & Books</span>
    						</div>
    					</div>
    					<div class="category3 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon4.png') ?>"><span>Valuable</span>
    						</div>
    					</div>
    					<div class="category4 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon1.png') ?>"><span>Entertainment</span>
    						</div>
    					</div>
    					<div class="category3 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon7.png') ?>"><span>Real Estate / Properties</span>
    						</div>
    					</div>
    				</div>
    				<div class="item">
    					<div class="category5 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon3.png') ?>"><span>Accessories</span>
    						</div>
    					</div>
    					<div class="category6 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon5.png') ?>"><span>Media</span>
    						</div>
    					</div>
    					<div class="category5 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon1.png') ?>"><span>Entertainment</span>
    						</div>
    					</div>
    					<div class="category6 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon3.png') ?>"><span>Accessories</span>
    						</div>
    					</div>
    					<div class="category5 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon4.png') ?>"><span>Valuable</span>
    						</div>
    					</div>
    				</div>
    				<div class="item">
    					<div class="category7 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon4.png') ?>"><span>Valuable</span>
    						</div>
    					</div>
    					<div class="category8 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon7.png') ?>"><span>Real Estate / Properties</span>
    						</div>
    					</div>
    					<div class="category7 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon6.png') ?>"><span>Academics & Books</span>
    						</div>
    					</div>
    					<div class="category8 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon4.png') ?>"><span>Valuable</span>
    						</div>
    					</div>
    					<div class="category7 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon6.png') ?>"><span>Academics & Books</span>
    						</div>
    					</div>
    				</div>
    				<div class="item">
    					<div class="category9 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon5.png') ?>"><span>Media</span>
    						</div>
    					</div>
    					<div class="category10 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon3.png') ?>"><span>Accessories</span>
    						</div>
    					</div>
    					<div class="category9 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon2.png') ?>"><span>Valuable</span>
    						</div>
    					</div>
    					<div class="category10 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon7.png') ?>"><span>Real Estate / Properties</span>
    						</div>
    					</div>
    					<div class="category9 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon1.png') ?>"><span>Entertainment</span>
    						</div>
    					</div>
    				</div>
    				<div class="item">
    					<div class="category11 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon1.png') ?>"><span>Entertainment</span>
    						</div>
    					</div>
    					<div class="category12 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon6.png') ?>"><span>Academics & Books</span>
    						</div>
    					</div>
    					<div class="category11 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon3.png') ?>"><span>Accessories</span>
    						</div>
    					</div>
    					<div class="category12 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon5.png') ?>"><span>Media</span>
    						</div>
    					</div>
    					<div class="category11 category">
    						<div>
    							<img src="<?= base_url('assets/img/icon6.png') ?>"><span>Academics & Books</span>
    						</div>
    					</div>
    				</div>
    			</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row heading">
				<div class="col-12 text-center">
					<h3>BUSINESS DIRECTORY</h3>
					<hr>
				</div>
			</div>
		</div>
		<div class="clearfix"></div><br><br><br><br>
		<div class="container">
			<div class="row popular_services">
				<div class="col-lg-6 col-md-12 col-sm-12 col-12">
					<div class="service_section">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-4">
								<div class="text-center company_logo">
									<img src="<?= base_url('assets/img/company_logo.png') ?>" class="img-fluid mx-auto d-block">
									<h5>COMPANY</h5>
									<small>TAG LINE GOES HERE</small>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-8 details">
								<div class="float-left">
									<div class="icon">
										<img src="<?= base_url('assets/img/phone.png') ?>">
									</div>
								</div>
								<div class="float-left mb20">
									<p>012 345 6789</p>
									<p>012 345 6789</p>
								</div>
								<div class="clearfix"></div>
								<div class="float-left">
									<div class="icon">
										<img src="<?= base_url('assets/img/web.png') ?>">
									</div>
								</div>
								<div class="float-left mb20">
									<p>info@websiteurl.com</p>
									<p>www.websiteurl.com</p>
								</div>
								<div class="clearfix"></div>
								<div class="float-left">
									<div class="icon">
										<img src="<?= base_url('assets/img/location.png') ?>">
									</div>
								</div>
								<div class="float-left mb20">
									<p>3222 Blackwell Street</p>
									<p>Fairbanks, AK 99701</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12 col-sm-12 col-12">
					<div class="service_section">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-4">
								<div class="text-center company_logo">
									<img src="<?= base_url('assets/img/company_logo.png') ?>" class="img-fluid mx-auto d-block">
									<h5>COMPANY</h5>
									<small>TAG LINE GOES HERE</small>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-8 details">
								<div class="float-left">
									<div class="icon">
										<img src="<?= base_url('assets/img/phone.png') ?>">
									</div>
								</div>
								<div class="float-left mb20">
									<p>012 345 6789</p>
									<p>012 345 6789</p>
								</div>
								<div class="clearfix"></div>
								<div class="float-left">
									<div class="icon">
										<img src="<?= base_url('assets/img/web.png') ?>">
									</div>
								</div>
								<div class="float-left mb20">
									<p>info@websiteurl.com</p>
									<p>www.websiteurl.com</p>
								</div>
								<div class="clearfix"></div>
								<div class="float-left">
									<div class="icon">
										<img src="<?= base_url('assets/img/location.png') ?>">
									</div>
								</div>
								<div class="float-left mb20">
									<p>3222 Blackwell Street</p>
									<p>Fairbanks, AK 99701</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12 col-sm-12 col-12">
					<div class="service_section">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-4">
								<div class="text-center company_logo">
									<img src="<?= base_url('assets/img/company_logo.png') ?>" class="img-fluid mx-auto d-block">
									<h5>COMPANY</h5>
									<small>TAG LINE GOES HERE</small>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-8 details">
								<div class="float-left">
									<div class="icon">
										<img src="<?= base_url('assets/img/phone.png') ?>">
									</div>
								</div>
								<div class="float-left mb20">
									<p>012 345 6789</p>
									<p>012 345 6789</p>
								</div>
								<div class="clearfix"></div>
								<div class="float-left">
									<div class="icon">
										<img src="<?= base_url('assets/img/web.png') ?>">
									</div>
								</div>
								<div class="float-left mb20">
									<p>info@websiteurl.com</p>
									<p>www.websiteurl.com</p>
								</div>
								<div class="clearfix"></div>
								<div class="float-left">
									<div class="icon">
										<img src="<?= base_url('assets/img/location.png') ?>">
									</div>
								</div>
								<div class="float-left mb20">
									<p>3222 Blackwell Street</p>
									<p>Fairbanks, AK 99701</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12 col-sm-12 col-12">
					<div class="service_section">
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6 col-4">
								<div class="text-center company_logo">
									<img src="<?= base_url('assets/img/company_logo.png') ?>" class="img-fluid mx-auto d-block">
									<h5>COMPANY</h5>
									<small>TAG LINE GOES HERE</small>
								</div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-8 details">
								<div class="float-left">
									<div class="icon">
										<img src="<?= base_url('assets/img/phone.png') ?>">
									</div>
								</div>
								<div class="float-left mb20">
									<p>012 345 6789</p>
									<p>012 345 6789</p>
								</div>
								<div class="clearfix"></div>
								<div class="float-left">
									<div class="icon">
										<img src="<?= base_url('assets/img/web.png') ?>">
									</div>
								</div>
								<div class="float-left mb20">
									<p>info@websiteurl.com</p>
									<p>www.websiteurl.com</p>
								</div>
								<div class="clearfix"></div>
								<div class="float-left">
									<div class="icon">
										<img src="<?= base_url('assets/img/location.png') ?>">
									</div>
								</div>
								<div class="float-left mb20">
									<p>3222 Blackwell Street</p>
									<p>Fairbanks, AK 99701</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row bottom_section">
				<div class="col-12">
					<hr>
				</div>
				<div class="col-lg-6 offset-lg-3 col-md-12 col-sm-12 col-12">
					<a href="#" class="btn btn-notify btn-block">
						GET NOTIFIED, REGISTER
					</a>
				</div>
			</div>
		</div>
		<div class="modal modal-wide fade-scale" id="categoryModal" tabindex="-1" role="dialog" aria-labelledby="categoryModalLabel">
  			<div class="modal-dialog modal-lg" role="document">
    			<div class="modal-content">
      				<div class="modal-header">
      					<h4>Entertainment	</h4>
      					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      				</div>
      				<div class="modal-body">
      					<div class="row">
							<div class="col-md-3">
      							<a href="<?= site_url('listing') ?>"><i class="fa fa-tag"></i>Restaurants</a>
      						</div>
        					<div class="col-md-3"><a href="<?= site_url('listing') ?>"><i class="fa fa-tag"></i>Restaurants</a></div>
        					<div class="col-md-3"><a href="#"><i class="fa fa-tag"></i>Restaurants</a></div>
        					<div class="col-md-3"><a href="#"><i class="fa fa-tag"></i>Restaurants</a></div>
        					<div class="col-md-3"><a href="#"><i class="fa fa-tag"></i>Restaurants</a></div>
        					<div class="col-md-3"><a href="#"><i class="fa fa-tag"></i>Restaurants</a></div>
        					<div class="col-md-3"><a href="<?= site_url('listing') ?>"><i class="fa fa-tag"></i>Restaurants</a></div>
        					<div class="col-md-3"><a href="#"><i class="fa fa-tag"></i>Restaurants</a></div>
        					<div class="col-md-3"><a href="#"><i class="fa fa-tag"></i>Restaurants</a></div>
        					<div class="col-md-3"><a href="#"><i class="fa fa-tag"></i>Restaurants</a></div>
        					<div class="col-md-3"><a href="#"><i class="fa fa-tag"></i>Restaurants</a></div>
        					<div class="col-md-3"><a href="#"><i class="fa fa-tag"></i>Restaurants</a></div>
        				</div>
      				</div>
    			</div>
  			</div>
		</div>