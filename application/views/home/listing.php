
		<section class="listing_section">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="tab" role="tabpanel">
			                <!-- Nav tabs -->
			                <ul class="nav nav-tabs" role="tablist">
			                    <li class="nav-item">
			                    	<a class="nav-link active" href="#company" role="tab" data-toggle="tab">COMPANY LISTING</a>
			                    </li>
			                    <li role="presentation" class="nav-item"><a class="nav-link" href="#business_cards" role="tab" data-toggle="tab">BUSINESS CARDS</a></li>
			                </ul>
			                <!-- Tab panes -->
			                <div class="tab-content">
			                    <div role="tabpanel" class="tab-pane fade in active show" id="company">
			                    	<h5>Search By:</h5>
			                    	<form>
			                    		<div class="row">
			                    			<div class="col-md-12">
			                    				<div class="search-area">
						                    		<div class="col-md-3 form-group nopadding">
						                    			<select class="form-control state" name="state">
						                    				<option disabled="" selected="">State</option>
						                    				<option value="Kerala">Kerala</option>
						                    				<option value="Tamilnadu">Tamilnadu</option>
						                    			</select>
						                    		</div>
						                    		<div class="col-md-3 form-group nopadding">
						                    			<select class="form-control" name="district">
						                    				<option disabled="" selected="">District</option>
						                    				<option value="Ernakulam">Ernakulam</option>
						                    				<option value="Trivandrum">Trivandrum</option>
						                    			</select>
						                    		</div>
						                    		<div class="col-md-3 form-group nopadding">
						                    			<input type="text" name="city" class="form-control" placeholder="City">
						                    		</div>
						                    		<div class="col-md-3 form-group nopadding">
						                    			<button class="btn btn-srch btn-block">Search</button>
						                    		</div>
						                    	</div>
					                    	</div>
			                    		</div>
			                    	</form>
			                    	<hr>
			                    	<div class="row" style="margin-top: 25px;">
				                        <div class="col-lg-6 col-md-12 col-sm-12 col-12">
				                        	<a href="<?= site_url('company') ?>">
												<div class="service_section">
													<div class="row">
														<div class="col-lg-6 col-md-6 col-sm-6 col-4">
															<div class="text-center company_logo">
																<img src="<?= base_url('assets/img/company_logo.png') ?>" class="img-fluid mx-auto d-block">
																<h5>COMPANY</h5>
																<small>TAG LINE GOES HERE</small>
															</div>
														</div>
														<div class="col-lg-6 col-md-6 col-sm-6 col-8 details">
															<div class="float-left">
																<div class="icon">
																	<img src="<?= base_url('assets/img/phone.png') ?>">
																</div>
															</div>
															<div class="float-left mb20">
																<p>012 345 6789</p>
																<p>012 345 6789</p>
															</div>
															<div class="clearfix"></div>
															<div class="float-left">
																<div class="icon">
																	<img src="<?= base_url('assets/img/web.png') ?>">
																</div>
															</div>
															<div class="float-left mb20">
																<p>info@websiteurl.com</p>
																<p>www.websiteurl.com</p>
															</div>
															<div class="clearfix"></div>
															<div class="float-left">
																<div class="icon">
																	<img src="<?= base_url('assets/img/location.png') ?>">
																</div>
															</div>
															<div class="float-left mb20">
																<p>3222 Blackwell Street</p>
																<p>Fairbanks, AK 99701</p>
															</div>
														</div>
													</div>
												</div>
											</a>
										</div>
										<div class="col-lg-6 col-md-12 col-sm-12 col-12">
				                        	<a href="<?= site_url('company') ?>">
												<div class="service_section">
													<div class="row">
														<div class="col-lg-6 col-md-6 col-sm-6 col-4">
															<div class="text-center company_logo">
																<img src="<?= base_url('assets/img/company_logo.png') ?>" class="img-fluid mx-auto d-block">
																<h5>COMPANY</h5>
																<small>TAG LINE GOES HERE</small>
															</div>
														</div>
														<div class="col-lg-6 col-md-6 col-sm-6 col-8 details">
															<div class="float-left">
																<div class="icon">
																	<img src="<?= base_url('assets/img/phone.png') ?>">
																</div>
															</div>
															<div class="float-left mb20">
																<p>012 345 6789</p>
																<p>012 345 6789</p>
															</div>
															<div class="clearfix"></div>
															<div class="float-left">
																<div class="icon">
																	<img src="<?= base_url('assets/img/web.png') ?>">
																</div>
															</div>
															<div class="float-left mb20">
																<p>info@websiteurl.com</p>
																<p>www.websiteurl.com</p>
															</div>
															<div class="clearfix"></div>
															<div class="float-left">
																<div class="icon">
																	<img src="<?= base_url('assets/img/location.png') ?>">
																</div>
															</div>
															<div class="float-left mb20">
																<p>3222 Blackwell Street</p>
																<p>Fairbanks, AK 99701</p>
															</div>
														</div>
													</div>
												</div>
											</a>
										</div>
										<div class="col-lg-6 col-md-12 col-sm-12 col-12">
				                        	<a href="<?= site_url('company') ?>">
												<div class="service_section">
													<div class="row">
														<div class="col-lg-6 col-md-6 col-sm-6 col-4">
															<div class="text-center company_logo">
																<img src="<?= base_url('assets/img/company_logo.png') ?>" class="img-fluid mx-auto d-block">
																<h5>COMPANY</h5>
																<small>TAG LINE GOES HERE</small>
															</div>
														</div>
														<div class="col-lg-6 col-md-6 col-sm-6 col-8 details">
															<div class="float-left">
																<div class="icon">
																	<img src="<?= base_url('assets/img/phone.png') ?>">
																</div>
															</div>
															<div class="float-left mb20">
																<p>012 345 6789</p>
																<p>012 345 6789</p>
															</div>
															<div class="clearfix"></div>
															<div class="float-left">
																<div class="icon">
																	<img src="<?= base_url('assets/img/web.png') ?>">
																</div>
															</div>
															<div class="float-left mb20">
																<p>info@websiteurl.com</p>
																<p>www.websiteurl.com</p>
															</div>
															<div class="clearfix"></div>
															<div class="float-left">
																<div class="icon">
																	<img src="<?= base_url('assets/img/location.png') ?>">
																</div>
															</div>
															<div class="float-left mb20">
																<p>3222 Blackwell Street</p>
																<p>Fairbanks, AK 99701</p>
															</div>
														</div>
													</div>
												</div>
											</a>
										</div>
										<div class="col-lg-6 col-md-12 col-sm-12 col-12">
				                        	<a href="<?= site_url('company') ?>">
												<div class="service_section">
													<div class="row">
														<div class="col-lg-6 col-md-6 col-sm-6 col-4">
															<div class="text-center company_logo">
																<img src="<?= base_url('assets/img/company_logo.png') ?>" class="img-fluid mx-auto d-block">
																<h5>COMPANY</h5>
																<small>TAG LINE GOES HERE</small>
															</div>
														</div>
														<div class="col-lg-6 col-md-6 col-sm-6 col-8 details">
															<div class="float-left">
																<div class="icon">
																	<img src="<?= base_url('assets/img/phone.png') ?>">
																</div>
															</div>
															<div class="float-left mb20">
																<p>012 345 6789</p>
																<p>012 345 6789</p>
															</div>
															<div class="clearfix"></div>
															<div class="float-left">
																<div class="icon">
																	<img src="<?= base_url('assets/img/web.png') ?>">
																</div>
															</div>
															<div class="float-left mb20">
																<p>info@websiteurl.com</p>
																<p>www.websiteurl.com</p>
															</div>
															<div class="clearfix"></div>
															<div class="float-left">
																<div class="icon">
																	<img src="<?= base_url('assets/img/location.png') ?>">
																</div>
															</div>
															<div class="float-left mb20">
																<p>3222 Blackwell Street</p>
																<p>Fairbanks, AK 99701</p>
															</div>
														</div>
													</div>
												</div>
											</a>
										</div>
										<div class="col-lg-6 col-md-12 col-sm-12 col-12">
				                        	<a href="<?= site_url('company') ?>">
												<div class="service_section">
													<div class="row">
														<div class="col-lg-6 col-md-6 col-sm-6 col-4">
															<div class="text-center company_logo">
																<img src="<?= base_url('assets/img/company_logo.png') ?>" class="img-fluid mx-auto d-block">
																<h5>COMPANY</h5>
																<small>TAG LINE GOES HERE</small>
															</div>
														</div>
														<div class="col-lg-6 col-md-6 col-sm-6 col-8 details">
															<div class="float-left">
																<div class="icon">
																	<img src="<?= base_url('assets/img/phone.png') ?>">
																</div>
															</div>
															<div class="float-left mb20">
																<p>012 345 6789</p>
																<p>012 345 6789</p>
															</div>
															<div class="clearfix"></div>
															<div class="float-left">
																<div class="icon">
																	<img src="<?= base_url('assets/img/web.png') ?>">
																</div>
															</div>
															<div class="float-left mb20">
																<p>info@websiteurl.com</p>
																<p>www.websiteurl.com</p>
															</div>
															<div class="clearfix"></div>
															<div class="float-left">
																<div class="icon">
																	<img src="<?= base_url('assets/img/location.png') ?>">
																</div>
															</div>
															<div class="float-left mb20">
																<p>3222 Blackwell Street</p>
																<p>Fairbanks, AK 99701</p>
															</div>
														</div>
													</div>
												</div>
											</a>
										</div>
										<div class="col-lg-6 col-md-12 col-sm-12 col-12">
				                        	<a href="<?= site_url('company') ?>">
												<div class="service_section">
													<div class="row">
														<div class="col-lg-6 col-md-6 col-sm-6 col-4">
															<div class="text-center company_logo">
																<img src="<?= base_url('assets/img/company_logo.png') ?>" class="img-fluid mx-auto d-block">
																<h5>COMPANY</h5>
																<small>TAG LINE GOES HERE</small>
															</div>
														</div>
														<div class="col-lg-6 col-md-6 col-sm-6 col-8 details">
															<div class="float-left">
																<div class="icon">
																	<img src="<?= base_url('assets/img/phone.png') ?>">
																</div>
															</div>
															<div class="float-left mb20">
																<p>012 345 6789</p>
																<p>012 345 6789</p>
															</div>
															<div class="clearfix"></div>
															<div class="float-left">
																<div class="icon">
																	<img src="<?= base_url('assets/img/web.png') ?>">
																</div>
															</div>
															<div class="float-left mb20">
																<p>info@websiteurl.com</p>
																<p>www.websiteurl.com</p>
															</div>
															<div class="clearfix"></div>
															<div class="float-left">
																<div class="icon">
																	<img src="<?= base_url('assets/img/location.png') ?>">
																</div>
															</div>
															<div class="float-left mb20">
																<p>3222 Blackwell Street</p>
																<p>Fairbanks, AK 99701</p>
															</div>
														</div>
													</div>
												</div>
											</a>
										</div>
									</div>
			                    </div>
			                    <div role="tabpanel" class="tab-pane fade" id="business_cards">
			                    	<h5>Search By:</h5>
			                    	<form>
			                    		<div class="row">
			                    			<div class="col-md-12">
			                    				<div class="search-area">
						                    		<div class="col-md-3 form-group nopadding">
						                    			<select class="form-control state" name="state">
						                    				<option disabled="" selected="">State</option>
						                    				<option value="Kerala">Kerala</option>
						                    				<option value="Tamilnadu">Tamilnadu</option>
						                    			</select>
						                    		</div>
						                    		<div class="col-md-3 form-group nopadding">
						                    			<select class="form-control" name="district">
						                    				<option disabled="" selected="">District</option>
						                    				<option value="Ernakulam">Ernakulam</option>
						                    				<option value="Trivandrum">Trivandrum</option>
						                    			</select>
						                    		</div>
						                    		<div class="col-md-3 form-group nopadding">
						                    			<input type="text" name="city" class="form-control" placeholder="City">
						                    		</div>
						                    		<div class="col-md-3 form-group nopadding">
						                    			<button class="btn btn-srch btn-block">Search</button>
						                    		</div>
						                    	</div>
						                    </div>
			                    		</div>
			                    	</form>
			                    	<hr>
			                    	<div class="row" style="margin-top: 25px;">
				                        <div class="col-lg-6 col-md-12 col-sm-12 col-12">
				                        	<a href="company.php">
												<div class="service_section business_cards">
													<div class="row">
														<div class="col-lg-6 col-md-6 col-sm-6 col-12">
															<div class="text-center company_logo">
																<img src="<?= base_url('assets/img/company_logo.png') ?>" class="img-fluid mx-auto d-block">
																<h5>COMPANY</h5>
																<small>TAG LINE GOES HERE</small>
																<div class="clearfix"></div><br>
																<div class="company_addr">
																	<div class="float-left">
																		<div class="icon">
																			<img src="<?= base_url('assets/img/location.png') ?>">
																		</div>
																	</div>
																	<div class="float-left mb20">
																		<p>3222 Blackwell Street</p>
																		<p>Fairbanks, AK 99701</p>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-lg-6 col-md-6 col-sm-6 col-12 details">
															<div class="float-left">
																<div class="icon1">
																	<img src="<?= base_url('assets/img/user.png') ?>">
																</div>
															</div>
															<div class="float-left name_section">
																<h4>MICHAL JOHNS</h4>
																<span>Solution Manager</span>
															</div>
															<div class="clearfix"></div>
															<div class="float-left">
																<div class="icon">
																	<img src="<?= base_url('assets/img/phone.png') ?>">
																</div>
															</div>
															<div class="float-left mb20">
																<p>012 345 6789</p>
																<p>012 345 6789</p>
															</div>
															<div class="clearfix"></div>
															<div class="float-left">
																<div class="icon">
																	<img src="<?= base_url('assets/img/web.png') ?>">
																</div>
															</div>
															<div class="float-left mb20">
																<p>info@websiteurl.com</p>
																<p>www.websiteurl.com</p>
															</div>
															<div class="clearfix"></div>
															<div class="mob_addr">
																<div class="float-left">
																	<div class="icon">
																		<img src="<?= base_url('assets/img/location.png') ?>">
																	</div>
																</div>
																<div class="float-left mb20">
																	<p>3222 Blackwell Street</p>
																	<p>Fairbanks, AK 99701</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</a>
										</div>
										<div class="col-lg-6 col-md-12 col-sm-12 col-12">
											<a href="company.php">
												<div class="service_section business_cards">
													<div class="row">
														<div class="col-lg-6 col-md-6 col-sm-6 col-12">
															<div class="text-center company_logo">
																<img src="<?= base_url('assets/img/company_logo.png') ?>" class="img-fluid mx-auto d-block">
																<h5>COMPANY</h5>
																<small>TAG LINE GOES HERE</small>
																<div class="clearfix"></div><br>
																<div class="company_addr">
																	<div class="float-left">
																		<div class="icon">
																			<img src="<?= base_url('assets/img/location.png') ?>">
																		</div>
																	</div>
																	<div class="float-left mb20">
																		<p>3222 Blackwell Street</p>
																		<p>Fairbanks, AK 99701</p>
																	</div>
																</div>
															</div>
														</div>
														<div class="col-lg-6 col-md-6 col-sm-6 col-12 details">
															<div class="float-left">
																<div class="icon1">
																	<img src="<?= base_url('assets/img/user.png') ?>">
																</div>
															</div>
															<div class="float-left name_section">
																<h4>MICHAL JOHNS</h4>
																<span>Solution Manager</span>
															</div>
															<div class="clearfix"></div>
															<div class="float-left">
																<div class="icon">
																	<img src="<?= base_url('assets/img/phone.png') ?>">
																</div>
															</div>
															<div class="float-left mb20">
																<p>012 345 6789</p>
																<p>012 345 6789</p>
															</div>
															<div class="clearfix"></div>
															<div class="float-left">
																<div class="icon">
																	<img src="<?= base_url('assets/img/web.png') ?>">
																</div>
															</div>
															<div class="float-left mb20">
																<p>info@websiteurl.com</p>
																<p>www.websiteurl.com</p>
															</div>
															<div class="clearfix"></div>
															<div class="mob_addr">
																<div class="float-left">
																	<div class="icon">
																		<img src="<?= base_url('assets/img/location.png') ?>">
																	</div>
																</div>
																<div class="float-left mb20">
																	<p>3222 Blackwell Street</p>
																	<p>Fairbanks, AK 99701</p>
																</div>
															</div>
														</div>
													</div>
												</div>
											</a>
										</div>
									</div>
			                    </div>
			                </div>
			            </div>
					</div>
				</div>
			</div>
		</section>
		