		<header class="header">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-6 col-6">
						<a href="<?= site_url() ?>"><img src="<?= base_url('assets/img/logo.png') ?>" class="img-fluid logo"></a>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-6 col-6 login">
						<span class="float-right">
							<a href="#" data-toggle="modal" data-target="#loginModal"><i class="fa fa-user"></i> Login</a>
						</span>
					</div>
				</div>
			</div>
		</header>