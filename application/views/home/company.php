		<section id="mainSlider" class="company_slider">
			<div id="owl-main" class="owl-carousel height-md owl-inner-nav owl-ui-lg">
				<div class="item" style="background-image: url(<?= base_url('assets/img/slider.jpg') ?>);">
				</div><!-- /.item -->
				<div class="item" style="background-image: url(<?= base_url('assets/img/slider.jpg') ?>);">
				</div><!-- /.item -->
			</div><!-- /.owl-carousel -->
		</section>
		<section>
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="company_info">
							<img src="<?= base_url('assets/img/company_logo.png') ?>" class="img-fluid mx-auto d-block">
							<h4>COMAPNY NAME</h4>
							<h5>Contact :</h5>
							<p>Phone: &emsp;(01) 492910 341<br>&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(01) 492910 341</p>
							<p>Fax: &emsp;&emsp;&nbsp;(01) 492910 341</p>
							<p>Mail: &emsp;&emsp;info@reach2us.com</p>
							<div class="clearfix"></div><br>
							<h5>Address :</h5>
							<p>Address line 1,</p>
							<p>Address line 2,</p>
							<p>Address line 3,</p>
						</div>
						<div class="newsletter_subscribe">
							<h5>SUBSCRIBE TO NEWSLETTER</h5>
							<div class="form-group subscribe">
								<input type="email" name="email" placeholder="Enter Your Email" class="form-control">
								<button class="btn btn-subscribe">SUBMIT</button>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="about_company">
							<h5><b>About</b></h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
							proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
							<div class="clearfix"></div><br>
							<h5><b>Follow Us:</b></h5>
							<a href="#"><i class="fa fa-facebook"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-linkedin"></i></a>
							<a href="#"><i class="fa fa-google-plus"></i></a>
							<div class="clearfix"></div><br>
							<div class="row">
								<div class="col-md-6">
									<h5><b>Branches:</b></h5>
									<h6><b>Address :</b></h6>
									<p>Address Line 1,</p>
									<p>Address Line 2,</p>
									<p>Address Line 3</p>
									<h6><b>Address :</b></h6>
									<p>Address Line 1,</p>
									<p>Address Line 2,</p>
									<p>Address Line 3</p>
								</div>
								<div class="col-md-6">
									<img src="<?= base_url('assets/img/rest.jpg') ?>" class="img-fluid">
								</div>
							</div>
							<div class="clearfix"></div><br>
						</div>
					</div>
				</div>
				<div class="clearfix"></div><br>	
				<div class="row">
					<div class="col-md-10 offset-md-1">
						<div class="row">
							<div class="col-md-6">
								<h5><b style="margin-right: 10px;position: relative;top: -10px ">Customer Rating:</b>
								<input type="hidden" class="rating" data-readonly value="3.5" data-filled="fa fa-star fa-2x" data-empty="fa fa-star-o fa-2x"/> </h5>
							</div>
							<div class="col-md-6">
								<button class="btn btn-rate btn-block">Rate us now</button>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<hr style="border-color: #0d4d95;border-width: 2px;width: 90%;margin-top: 30px;margin-bottom: 30px;">
				<div class="clearfix"></div>
				<div class="row">
					<div class="col-md-12">
						<h4 class="text-center">Location Map</h4>
						<div class="clearfix"></div><br>
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3929.4701455417935!2d76.29479901434247!3d9.977967292866243!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3b080d35b5772b59%3A0x4dd432e402832f80!2sSoarMorrow+Solutions+Pvt.+Ltd.!5e0!3m2!1sen!2sin!4v1535614003526" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				</div>
				<hr style="border-color: #0d4d95;border-width: 2px;width: 90%;margin-top: 30px;margin-bottom: 30px;">
				<div class="row company_tab">
					<div class="col-12">
						<div class="tab" role="tabpanel">
			                <!-- Nav tabs -->
			                <ul class="nav nav-tabs" role="tablist">
			                    <li class="nav-item">
			                    	<a class="nav-link active" href="#service" role="tab" data-toggle="tab">SERVICES</a>
			                    </li>
			                    <li role="presentation" class="nav-item"><a class="nav-link" href="#gallery" role="tab" data-toggle="tab">GALLERY</a></li>
			                    <li role="presentation" class="nav-item"><a class="nav-link" href="#attachment" role="tab" data-toggle="tab">ATTACHMENT</a></li>
			                    <li role="presentation" class="nav-item"><a class="nav-link" href="#contact" role="tab" data-toggle="tab">CONTACTS</a></li>
			                    <li role="presentation" class="nav-item"><a class="nav-link" href="#suggestion" role="tab" data-toggle="tab">SUGGESTIONS</a></li>
			                    <li role="presentation" class="nav-item"><a class="nav-link" href="#enquiry" role="tab" data-toggle="tab">ENQUIRY</a></li>
			                </ul>
			                <div class="tab-content">
			                    <div role="tabpanel" class="tab-pane fade in active show" id="service">
			                    	<div class="row">
			                    		<div class="col-md-3">
			                    			<img src="<?= base_url('assets/img/rest.jpg') ?>" class="img-fluid">
			                    			<h5>Restaurant</h5>
			                    		</div>
			                    		<div class="col-md-3">
			                    			<img src="<?= base_url('assets/img/rest.jpg') ?>" class="img-fluid">
			                    			<h5>Restaurant</h5>
			                    		</div>
			                    		<div class="col-md-3">
			                    			<img src="<?= base_url('assets/img/rest.jpg') ?>" class="img-fluid">
			                    			<h5>Restaurant</h5>
			                    		</div>
			                    		<div class="col-md-3">
			                    			<img src="<?= base_url('assets/img/rest.jpg') ?>" class="img-fluid">
			                    			<h5>Restaurant</h5>
			                    		</div>
			                    	</div>
			                    </div>
			                    <div role="tabpanel" class="tab-pane fade" id="gallery">
			                    	<div class="row">
			                    		<div id="gallery_img">
			                    			<a href="<?= base_url('assets/img/rest.jpg') ?>">
					                    		<div class="col-md-3">
					                    			<img src="<?= base_url('assets/img/rest.jpg') ?>" class="img-fluid">
					                    			<h5>Image</h5>
					                    		</div>
					                    	</a>
					                    	<a href="<?= base_url('assets/img/rest.jpg') ?>">
					                    		<div class="col-md-3">
					                    			<img src="<?= base_url('assets/img/rest.jpg') ?>" class="img-fluid">
					                    			<h5>Image</h5>
					                    		</div>
					                    	</a>
					                    	<a href="<?= base_url('assets/img/rest.jpg') ?>">
					                    		<div class="col-md-3">
					                    			<img src="<?= base_url('assets/img/rest.jpg') ?>" class="img-fluid">
					                    			<h5>Image</h5>
					                    		</div>
					                    	</a>
					                    	<a href="<?= base_url('assets/img/rest.jpg') ?>">
					                    		<div class="col-md-3">
					                    			<img src="<?= base_url('assets/img/rest.jpg') ?>" class="img-fluid">
					                    			<h5>Image</h5>
					                    		</div>
					                    	</a>
				                    	</div>
			                    	</div>
			                    </div>
			                    <div role="tabpanel" class="tab-pane fade" id="attachment">
			                    	<div class="row">
			                    		<div id="attachment_img">
			                    			<a href="<?= base_url('assets/img/rest.jpg') ?>">
					                    		<div class="col-md-3">
					                    			<img src="<?= base_url('assets/img/rest.jpg') ?>" class="img-fluid">
					                    			<h5>Image</h5>
					                    		</div>
					                    	</a>
					                    	<a href="<?= base_url('assets/img/rest.jpg') ?>">
					                    		<div class="col-md-3">
					                    			<img src="<?= base_url('assets/img/rest.jpg') ?>" class="img-fluid">
					                    			<h5>Image</h5>
					                    		</div>
					                    	</a>
					                    	<a href="<?= base_url('assets/img/rest.jpg') ?>">
					                    		<div class="col-md-3">
					                    			<img src="<?= base_url('assets/img/rest.jpg') ?>" class="img-fluid">
					                    			<h5>Image</h5>
					                    		</div>
					                    	</a>
					                    	<a href="<?= base_url('assets/img/rest.jpg') ?>">
					                    		<div class="col-md-3">
					                    			<img src="<?= base_url('assets/img/rest.jpg') ?>" class="img-fluid">
					                    			<h5>Image</h5>
					                    		</div>
					                    	</a>
				                    	</div>
			                    	</div>
			                    </div>
			                    <div role="tabpanel" class="tab-pane fade" id="contact">
		                    		<div class="col-md-6 companyInfo">
		                    			<img src="<?= base_url('assets/img/logo.png') ?>" class="img-fluid">
		                    			<div class="contactDetails">
		                    				<h4>Company Name</h4>
		                    				<h4>Place</h4>
		                    				<table>
		                    					<tbody>
		                    						<tr>
		                    							<th>Phone</th>
		                    							<td>1234568890</td>
		                    						</tr>
		                    						<tr>
		                    							<th>Email</th>
		                    							<td>companyname@gmail.com</td>
		                    						</tr>
		                    						<tr>
		                    							<th>Time</th>
		                    							<td>10:00 - 5:00</td>
		                    						</tr>
		                    					</tbody>
		                    				</table>
		                    			</div>
		                    		</div>
		                    		<div class="col-md-6 companyInfo">
		                    			<img src="<?= base_url('assets/img/logo.png') ?>" class="img-fluid">
		                    			<div class="contactDetails">
		                    				<h4>Company Name</h4>
		                    				<h4>Place</h4>
		                    				<table>
		                    					<tbody>
		                    						<tr>
		                    							<th>Phone</th>
		                    							<td>1234568890</td>
		                    						</tr>
		                    						<tr>
		                    							<th>Email</th>
		                    							<td>companyname@gmail.com</td>
		                    						</tr>
		                    						<tr>
		                    							<th>Time</th>
		                    							<td>10:00 - 5:00</td>
		                    						</tr>
		                    					</tbody>
		                    				</table>
		                    			</div>
		                    		</div>
			                    </div>
			                    <div role="tabpanel" class="tab-pane fade" id="suggestion">
			                    	<div class="row">
			                    		<div class="col-md-8">
					                    	<h4>Write a review</h4>
					                    	<form>
					                    		<div class="form-group">
					                    			<label>TITLE</label>
					                    			<input type="text" name="title" class="form-control">
					                    		</div>
					                    		<div class="form-group">
					                    			<label>MESSAGE</label>
					                    			<textarea name="message" class="form-control" rows="6"></textarea>
					                    		</div>
					                    		<div class="form-group">
					                    			<button class="btn btn-contact float-right">SUBMIT</button>
					                    		</div>
					                    	</form>
					                    </div>
					                    <div class="col-md-4">
					                    	<h4>Rating</h4>
					                    	<h6>Please Rate Us:</h6>
					                    	<input type="hidden" class="rating-tooltip-manual" data-filled="fa fa-star fa-2x" data-empty="fa fa-star-o fa-2x" data-fractions="4"/>
					                    </div>
					                </div>
			                    </div>
			                    <div role="tabpanel" class="tab-pane fade" id="enquiry">
			                    	<div class="row">
			                    		<div class="col-md-6 offset-md-3">
			                    			<h4>Submit Your Enquiry</h4>
			                    			<form>
			                    				<div class="form-group">
			                    					<label>NAME</label>
			                    					<input type="text" name="name" class="form-control">
			                    				</div>
			                    				<div class="form-group">
			                    					<label>EMAIL</label>
			                    					<input type="text" name="email" class="form-control">
			                    				</div>
			                    				<div class="form-group">
			                    					<label>MESSAGE</label>
			                    					<textarea rows="6" class="form-control" name="message"></textarea>
			                    				</div>
					                    		<div class="form-group">
					                    			<button class="btn btn-contact float-right">SUBMIT</button>
					                    		</div>
			                    			</form>
			                    		</div>
			                    	</div>
			                    </div>
			                </div>
			            </div>
					</div>
				</div>
			</div>
		</section>
		<div class="container">
			<div class="row bottom_section">
				<div class="col-12">
					<hr style="top: 0">
				</div>
				<div class="col-lg-6 offset-lg-3 col-md-12 col-sm-12 col-12">
					<a href="#" class="btn btn-notify btn-block" style="top: 20px;">
						GET NOTIFIED ABOUT US
					</a>
				</div>
			</div>
		</div>
		<div class="clearfix"></div><br><br><br>
		