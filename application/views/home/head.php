<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Reach2us</title>
		<link rel="shortcut icon" href="" type="image/x-icon">
		<link rel="icon" href="" type="image/x-icon">
		<!-- Responsive -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
		<link href="<?= base_url('assets/img/favicon.png') ?>" type="image/x-icon" rel="shortcut icon">
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css">
		<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/jquery.owlcarousel/1.31/owl.carousel.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/style.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/icheck-bootstrap.min.css') ?>">
		<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.2/css/lightgallery.min.css">
	</head>
	<body>