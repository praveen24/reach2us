		<footer>
			<div class="container">
				<div class="row">
					<div class="col-lg-4 col-md-4 col-sm-12 col-12">
						<h3>Popular</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-12">
						<h3>Contact Us</h3>
						<p>Address Line 1,</p>
						<p>Address Line 2,</p>
						<p>Address Line 3,</p>
						<div class="clearfix"></div>
						<p>Phone: &emsp;(01) 492910 341<br>&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(01) 492910 341</p>
						<p>Fax: &emsp;&emsp;&nbsp;(01) 492910 341</p>
						<p>Mail: &emsp;&emsp;info@reach2us.com</p>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-12">
						<h3>Subscribe Newsletter</h3>
						<form action="">
							<div class="form-group">
								<input type="text" name="email" placeholder="Email:" class="form-control">
							</div>
							<div class="form-group">
								<button class="btn btn-login float-right">Subscribe</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="clearfix"></div><br><br>
			<div class="container-fluid">
				<div class="row sub_footer">
					<div class="container">
						<div class="row">
							<div class="col-lg-8 col-md-8 col-sm-12 col-12">
								<p>Copyright &copy; 2018 <span>Your best business supporter.</span> All Rights Reserved</p>
							</div>
							<div class="col-lg-4 col-md-4 col-sm-12 col-12">
								<nav aria-label="breadcrumb">
								  	<ol class="breadcrumb">
									    <li class="breadcrumb-item"><a href="<?= site_url('about') ?>">About</a></li>
									    <li class="breadcrumb-item"><a href="#">News</a></li>
									    <li class="breadcrumb-item"><a href="<?= site_url('privacy') ?>">Privacy</a></li>
									    <li class="breadcrumb-item"><a href="<?= site_url('contact') ?>">Contact Us</a></li>
									    <li class="breadcrumb-item"><a href="<?= site_url('terms') ?>">Terms</a></li>
								  	</ol>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<div class="modal modal-wide fade-scale" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="categoryModalLabel">
  			<div class="modal-dialog" role="document">
    			<div class="modal-content">
      				<div class="modal-body">
      					<div class="login_logo">
      						<img src="<?= base_url('assets/img/logo.png') ?>" class="img-fluid mx-auto d-block">
      					</div>
      					<h1 class="text-center">LOGIN</h1>
      					<form action="">
      						<div class="form-group">
      							<input type="text" name="username" class="form-control" placeholder="Username">
      						</div>
      						<div class="form-group">
      							<input type="password" name="password" class="form-control" placeholder="Password">
      						</div>
							<div class="form-group" style="display: inline-block;width: 100%">
								<div class="checkbox icheck-success float-left">
			                        <input type="checkbox" id="primary2" name="primary" />
			                        <label for="primary2" style="color: #fff;">Remember me</label>
			                    </div>
								<a href="<?= site_url('forgot_password') ?>" class="float-right" style="color: #fff;">Lost Your Password?</a>
							</div>
							<div class="form-group">
								<button class="btn btn-log btn-block">LOGIN</button>
							</div>
      					</form>
      				</div>
    			</div>
  			</div>
		</div>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.2/js/lightgallery-all.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
		<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.owlcarousel/1.31/owl.carousel.js"></script>
		<script type="text/javascript" src="<?= base_url('assets/js/bootstrap-rating.js') ?>"></script>
		<script type="text/javascript" src="<?= base_url('assets/js/script.js') ?>"></script>
	</body>
</html>